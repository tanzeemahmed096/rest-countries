fetch("https://restcountries.com/v3.1/all")
  .then((response) => {
    return response.json();
  })
  .then((jsonData) => {
    let index = sessionStorage.getItem("id");
    let mainDiv = document.getElementsByClassName("main");
    // console.log(mainDiv[0]);
    // console.log(Object.values(jsonData[index].name.nativeName)[0].common);
    let countryName = jsonData[index].name.common;
    let nativeName = Object.values(jsonData[index].name.nativeName)[0].official;
    let population = jsonData[index].population;
    let region = jsonData[index].region;
    let subRegion = jsonData[index].subregion;
    let capital = jsonData[index].capital[0];
    let tld = jsonData[index].tld[0];
    let currency = Object.values(jsonData[index].currencies)[0].name;
    let languageArr = Object.values(jsonData[index].languages);

    // Creating String of languages
    let languages = languageArr.join(", ");

    //Get all Borders
    let borderArr = jsonData[index].borders;

    //border buttons
    let buttons = getBorders()
      .then((borderCode) => {
        let buttons = "";
        // console.log(typeof borderArr);
        if (borderArr) {
      
          borderArr.forEach((border) => {
            // console.log(typeof borderCode[border]);
            borderCode[border];
            buttons += `<button
        type="button"
        class="country-name-btn btn btn-light mx-2 w-10"
      
      >
        ${borderCode[border]}
      </button>`;
    
          });
        }
        return buttons;
      })
      .then((buttons) => {
  
        let newHtmlData = `<div class="flag d-flex justify-content-center">
        <img src="${jsonData[index].flags.png}" alt="Country flag" class="w-100" />
      </div>
      <div class="raw-data py-4">
        <div class="details">
          <div class="country-data">
            <h1 class="country-name py-4 fw-bold">${countryName}</h1>
            <div class="native-name d-flex flex-row d-grid f-700">
              <span>Native name: </span>
              <p>${nativeName}</p>
            </div>
            <div class="population d-flex flex-row f-700">
              <span>Population: </span>
              <p>${population.toLocaleString("en-us")}</p>
            </div>
            <div class="region d-flex flex-row f-700">
              <span>Region:</span>
              <p>${region}</p>
            </div>
            <div class="sub-region d-flex flex-row f-700">
              <span>Sub Region: </span>
              <p>${subRegion}</p>
            </div>
            <div class="capital d-flex flex-row f-700">
              <span>Capital: </span>
              <p>${capital}</p>
            </div>
          </div>
  
          <div class="additinal-data py-4">
            <div class="level-domain d-flex flex-row f-700">
              <span>Top level Domain: </span>
              <p>${tld}</p>
            </div>
            <div class="currencies d-flex flex-row f-700">
              <span>Currencies: </span>
              <p>${currency}</p>
            </div>
            <div class="languages d-flex flex-row f-700">
              <span>Languages: </span>
              <p>${languages}</p>
            </div>
          </div>
        </div>
        <div class="border-countries d-flex flex-column pt-4">
          <span>Border Countries:</span>
          <div class="countries d-flex flex-row pt-2" onclick = "getCountry()">
          ${buttons}
          </div>
        </div>
      </div>`;
        mainDiv[0].innerHTML = newHtmlData;
      });
  });
// border countries
function getBorders() {
  return fetch("https://restcountries.com/v3.1/all")
    .then((response) => {
      return response.json();
    })
    .then((jsonData) => {
      // console.log(jsonData);
      let countryCode = jsonData.reduce((acc, element) => {
        let cca3 = element.cca3;
        let country = element.name.common;
        if (!acc.cca3) {
          acc[cca3] = country;
        }

        // console.log(obj);
        return acc;
      }, {});
      // console.log(countryCode);
      return countryCode;
    });
}
function getCountry(e) {
  document.addEventListener("click", getDetails);
  
}



      // For buttons    ///
function getDetails(event) {
  event.target.innerText
    // console.log(mainDiv[0]);
    // console.log(Object.values(jsonData[index].name.nativeName)[0].common);

  fetch("https://restcountries.com/v3.1/all")
  .then((response) => {
    return response.json();
  })
  .then((jsonData) => {

    let index = 0;

    for(let i = 0; i<jsonData.length; i++){
      if(jsonData[i].name.common.localeCompare(event.target.innerText) == 0){
        index = i;
        break;
      }
    }

    let mainDiv = document.getElementsByClassName("main");
    let countryName = jsonData[index].name.common;
    let nativeName = Object.values(jsonData[index].name.nativeName)[0].official;
    let population = jsonData[index].population;
    let region = jsonData[index].region;
    let subRegion = jsonData[index].subregion;
    let capital = jsonData[index].capital[0];
    let tld = jsonData[index].tld[0];
    let currency = Object.values(jsonData[index].currencies)[0].name;
    let languageArr = Object.values(jsonData[index].languages);

    // Creating String of languages
    let languages = languageArr.join(", ");

    //Get all Borders
    let borderArr = jsonData[index].borders;

    //border buttons
    let buttons = getBorders()
      .then((borderCode) => {
        let buttons = "";
        // console.log(typeof borderArr);
        if (borderArr) {
      
          borderArr.forEach((border) => {
            // console.log(typeof borderCode[border]);
            borderCode[border];
            buttons += `<button
        type="button"
        class="country-name-btn btn btn-light mx-2 w-10"
      
      >
        ${borderCode[border]}
      </button>`;
    
          });
        }
        return buttons;
      })
      .then((buttons) => {
  
        let newHtmlData = `<div class="flag d-flex justify-content-center">
        <img src="${jsonData[index].flags.png}" alt="Country flag" class="w-100" />
      </div>
      <div class="raw-data py-4">
        <div class="details">
          <div class="country-data">
            <h1 class="country-name py-4 fw-bold">${countryName}</h1>
            <div class="native-name d-flex flex-row d-grid f-700">
              <span>Native name: </span>
              <p>${nativeName}</p>
            </div>
            <div class="population d-flex flex-row f-700">
              <span>Population: </span>
              <p>${population.toLocaleString("en-us")}</p>
            </div>
            <div class="region d-flex flex-row f-700">
              <span>Region:</span>
              <p>${region}</p>
            </div>
            <div class="sub-region d-flex flex-row f-700">
              <span>Sub Region: </span>
              <p>${subRegion}</p>
            </div>
            <div class="capital d-flex flex-row f-700">
              <span>Capital: </span>
              <p>${capital}</p>
            </div>
          </div>
  
          <div class="additinal-data py-4">
            <div class="level-domain d-flex flex-row f-700">
              <span>Top level Domain: </span>
              <p>${tld}</p>
            </div>
            <div class="currencies d-flex flex-row f-700">
              <span>Currencies: </span>
              <p>${currency}</p>
            </div>
            <div class="languages d-flex flex-row f-700">
              <span>Languages: </span>
              <p>${languages}</p>
            </div>
          </div>
        </div>
        <div class="border-countries d-flex flex-column pt-4">
          <span>Border Countries:</span>
          <div class="countries d-flex flex-row pt-2" onclick = "getCountry()">
          ${buttons}
          </div>
        </div>
      </div>`;
        mainDiv[0].innerHTML = newHtmlData;
      });
  });
}