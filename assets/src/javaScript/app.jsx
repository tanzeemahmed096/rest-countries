let cardContent = document.querySelector(".cards");

fetch("https://restcountries.com/v3.1/all")
  .then((response) => {
    return response.json();
  })
  .then((dataJson) => {
    jsonData = dataJson; //Copying Data for external use
    // console.log(dataJson);
    let countryDetails = "";
    let i = 0;
    dataJson.forEach((data) => {
      countryDetails += `<a href= "./assets/src/html/detail-country.html">
      <div class="card" id="${i}" onclick = "storeId(${i})">
      <img
        class="card-img-top"
        src="${data.flags.png}"
        alt="Card image cap"
      />
      <div class="card-body">
        <div class="country-name">
            <h1>${data.name.common}</h1>
        </div>
        <div class="raw-data">
            <div class="population d-flex">
                <span class="font-weight-bold">Population: </span>
                <p class="pr-3">${data.population.toLocaleString("en-us")}</p>
            </div>
            <div class="region d-flex">
                <span class="font-weight-bold">Region: </span>
                <p class="pr-3">${data.region}</p>
            </div>
            <div class="capital d-flex">
                <span class="font-weight-bold">Capital: </span>
                <p class="pr-3">${data.capital}</p>
            </div>
        </div>
      </div>
    </div>
    </a>`;
      i += 1;
    });
    cardContent.innerHTML = countryDetails;
  })
  .catch((error) => {
    console.error(error);
  });
//Searching functionality
let searchEle = document.getElementById("search");
// console.log(searchEle);
searchEle.addEventListener("keydown", searchContent);

function searchContent(event) {
  let searchWord = event.target.value;
  let cards = document.getElementsByClassName("card");
  for (let i = 0; i < cards.length; i++) {
    let cardElements = cards[i].getElementsByClassName("card-body")[0];
    let countryElement = cardElements.getElementsByClassName("country-name")[0];
    let countryName = countryElement.getElementsByTagName("h1")[0].innerText;

    if (countryName.toUpperCase().indexOf(searchWord.toUpperCase()) > -1) {
      cards[i].style.display = "";
    } else {
      cards[i].style.display = "none";
    }
  }
}

// Filtering data on basis of Region
function filterRegion() {
  let filterValue = document.getElementById("region-options");
  // console.log(filterValue);
  let selectedOption = filterValue.options[filterValue.selectedIndex].text;

  let cards = document.getElementsByClassName("card");
  for (let i = 0; i < cards.length; i++) {
    let cardElements = cards[i].getElementsByClassName("card-body")[0];
    let rawElement = cardElements.getElementsByClassName("raw-data")[0];
    let regionElement = rawElement.getElementsByClassName("region")[0];

    let regionName = regionElement.getElementsByTagName("p")[0].innerText;
    // console.log(filterValue);
    if (
      regionName.toUpperCase().indexOf(selectedOption.toUpperCase()) > -1 ||
      selectedOption.localeCompare("Filter by Region") == 0
    ) {
      cards[i].style.display = "";
    } else {
      cards[i].style.display = "none";
    }
  }
}

//Click on country

function storeId(index) {
  sessionStorage.setItem("id", index);
}
